# Jupyter notebook image for developing & learning ESM/ESM-RISM in Quantum ESPRESSO

**Please note that this documentation is underconstruction.**

### Build the docker image locally
```bash
docker build -t jupyterdev:latest .
```
### Launch the jupyterlab
- With token
```bash
docker run --rm \
           -p 8888:8888 \
           -e GRANT_SUDO=yes \
           --user root \
           -e JUPYTER_ENABLE_LAB=yes \
           --name jupyterdev \
           -v $PWD/develop:/home/jovyan/develop \
           -v $PWD/notebook:/home/jovyan/notebook \
           jupyterdev:latest \
           start.sh jupyter lab
```
- Without token
```bash
docker run --rm \
           -p 8888:8888 \
           -e GRANT_SUDO=yes \
           --user root \
           -e JUPYTER_ENABLE_LAB=yes \
           --name jupyterdev \
           -v $PWD/develop:/home/jovyan/develop \
           -v $PWD/notebook:/home/jovyan/notebook \
           jupyterdev:latest \
           start.sh jupyter lab
           --NotebookApp.token=''
```

### Connecting to the container with bash
```bash
docker exec -it jupyterdev start.sh
```
